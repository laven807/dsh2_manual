# 代理B&C安装升级手册

# 版本记录

| 日期       | 版本号 | 更新记录                                                                 | 负责人 |
| :--------- | :----- | :----------------------------------------------------------------------- | :----- |
| 2022-09-06 | V1.0   | 初版                                                                     | 邹旋   |
| 2022-09-09 | V1.1   | 1. 根据代理C V1.0.3 版本修改代理C安装过程 <br>                           | 邹旋   |
| 2022-09-13 | V1.2   | 1. 补充代理C 配置文件 port mac更改步骤 <br> 2. 补充代理C mac地址生成工具 | 邹旋   |
| 2022-09-15 | V1.3   | 1. 修正代理C 隧道表老化时间配置                                          | 邹旋   |

# 注意事项
!!!warning
    **==本文中的涉及的基本所有操作（除创建dsh用户的操作），均强烈建议在 dsh 用户下执行！！！！！！==**

    **==本文中的涉及的基本所有操作（除创建dsh用户的操作），均强烈建议在 dsh 用户下执行！！！！！！==**

    **==本文中的涉及的基本所有操作（除创建dsh用户的操作），均强烈建议在 dsh 用户下执行！！！！！！==**


# 文件准备

- 环境包

    === "下载链接-gitlab"
        [centos_env](http://10.0.4.29:10082/sun.qingmeng/dpi-env/repository/master/archive.tar.gz)


    === "下载链接-服务器"
        路径：`root@10.17.1.195:/home/dsh/centos-env/archive.tar.gz`


    ```shell
    356cc450c511e369e32ecdc074f41399  archive.tar.gz
    ```

- iso镜像
  
    下载链接：`root@10.17.1.195:/home/iso/CentOS-7-x86_64-Everything-2009.iso`

    ```shell
    8f8c832372b4b6d84076847b8d99b2ad  CentOS-7-x86_64-Everything-2009.iso
    ```

- igb_uio文件
- 代理C软件包
- 代理B软件包



# 代理B&C运行环境搭建

## 创建dsh用户
以root用户登录后，执行以下命令

```shell 
useradd dsh
passwd dsh
# 密码可以统一为 embed123@E
```
## 修改dsh用户权限

```shell title="/etc/sudoers"
vim /etc/sudoers
#添加dsh行
dsh ALL=(ALL)   NOPASSWD: ALL
#Defaults    requiretty #注释掉本行

```

## 切换到dsh用户

**后续所有操作均在 `dsh` 用户下执行！！！**

**后续所有操作均在 `dsh` 用户下执行！！！**

**后续所有操作均在 `dsh` 用户下执行！！！**

```shell
su - dsh
```

## 创建相关目录

```shell
mkdir ~/iso
mkdir ~/centos_env
mkdir -p ~/proxy_b/version
mkdir -p ~/proxy_c/version
```

## 挂载本地iso镜像

- 上传获取的`iso`文件到设备的 `/home/dsh/iso`目录下

- 执行以下指令，挂载iso镜像

    ```shell
    sudo mount /home/dsh/iso/CentOS-7-x86_64-Everything-2009.iso /media/ -o loop
    ```

- 检查挂载结果

    ```shell
    # 检查
    [root@localhost iso]# df -h 
    Filesystem               Size  Used Avail Use% Mounted on
    /dev/mapper/centos-root   50G   14G   37G  28% /
    devtmpfs                  13G     0   13G   0% /dev
    tmpfs                     63G     0   63G   0% /dev/shm
    tmpfs                     63G   42M   63G   1% /run
    tmpfs                     63G     0   63G   0% /sys/fs/cgroup
    /dev/mapper/centos-home   58G   33G   25G  58% /home
    /dev/sda1                497M  159M  339M  32% /boot
    tmpfs                     13G  252K   13G   1% /run/user/1002
    tmpfs                     13G     0   13G   0% /run/user/0
    tmpfs                     13G     0   13G   0% /run/user/1003
    /dev/loop0               7.3G  7.3G     0 100% /media
    ```

- 备份 `repo` 文件

    ```shell
    cd /etc/yum.repo.d                  # 切换到yum源配置目录
    sudo mkdir CentOs-bak               # 新建一个yum源配置文件备份目录，避免误删
    sudo mv CentOS* ./CentOs-bak    		# 把系统自动yum源移动到备份文件夹
    ```

- 创建挂载本地iso的 `repo`文件

    ```shell title='/etc/yum.repos.d/iso_local.repo'
    sudo vi /etc/yum.repos.d/iso_local.repo
    [iso_local]
    name=iso_local
    baseurl=file:///media/
    enabled=1
    gpgcheck=0
    ```
    
- 关闭所有源

    ```shell
    sudo yum-config-manager --disable "*"
    ```

- 只开启本地iso源

    ```shell
    sudo yum-config-manager --enable "iso_local"
    ```

- 检查挂载结果

    ```shell
    [dsh@localhost iso]$ yum repolist enabled
    
    # 结果中只显示 iso_local
    ```

## 配置dpdk自动绑定环境

- 上传 `igb_uio`文件夹到 `/home/dsh` 目录下
- 执行以下指令，配置dpdk自动绑定环境

```shell
cd /lib/modules/$(uname -r)/extra/
sudo mkdir dpdk
sudo cp ~/igb_uio/$(uname -r).x86_64.igb_uio.ko dpdk/igb_uio.ko

cd /etc/modules-load.d/
sudo vim dpdk.conf输入igb_uio
sudo depmod
sudo modprobe igb_uio
lsmod | grep igb可进行查看是否安装成功
```

## 配置大页内存

### 修改内核参数

```shell title='/etc/default/grub'
sudo vim /etc/default/grub
# 在 GRUB_CMDLINE_LINUX 行后面，增加如下内容
default_hugepagesz=1G hugepagesz=1G hugepages=128

# 最终内容如下
[root@localhost ~]# cat /etc/default/grub 
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR="$(sed 's, release .*$,,g' /etc/system-release)"
GRUB_DEFAULT=saved
GRUB_DISABLE_SUBMENU=true
GRUB_TERMINAL="serial console"
GRUB_SERIAL_COMMAND="serial --speed=115200"
GRUB_CMDLINE_LINUX="crashkernel=auto rd.lvm.lv=centos/root rd.lvm.lv=centos/swap console=ttyS0,115200 default_hugepagesz=1G hugepagesz=1G hugepages=128"
GRUB_DISABLE_RECOVERY="true"

```

### 更新grub文件

判断系统是用 efi 还是 mbr启动，如果 `/sys/firmware`路径下，有 `efi`子目录，则为 efi启动，否则为 mbr启动

=== "efi启动"
    ```shell
    sudo grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
    ```

=== "mbr启动"
    ```shell
    sudo grub2-mkconfig -o /boot/grub2/grub.cfg
    ```
### 配置挂载项

```shell
sudo mkdir -p /mnt/huge_1GB 

sudo vim /etc/fstab
# 新增一行内容
nodev /mnt/huge_1GB hugetlbfs  pagesize=1GB  0 0
```

### 重启设备&检查

重启设备，重启完成后，执行以下指令检查结果，预期total显示128。

```shell
cat /proc/meminfo  | grep Huge 
```


## 安装环境包

- 将环境包上传到 `/home/dsh/centos_env`目录下

    ```shell
    [dsh@localhost centos-env]$ pwd
    /home/dsh/centos-env
    [dsh@localhost centos-env]$ ls
    archive.tar.gz
    ```

- 解压环境包

    ```shell
    cd ~/centos_env
    tar -xzvf archive.tar.gz -C . --strip-components=1
    ```

- 安装依赖包

    ```shell
    cd ~/centos_env
    sh install.sh
    # 时间较长，约30min左右
    # 不用理会过程中的报错
    ```




# 安装代理C

## 上传软件包

上传 `rpm` 包到 `/home/dsh/proxy_c/version` 目录下

## 安装rpm包

执行以下指令，进行rpm的首次安装

```shell
cd /home/dsh/proxy_c/verison
sudo rpm -ivh xxx.rpm  --nodeps --prefix=/home/dsh/.local

```


## 修改代理C配置文件

代理C配置文件路径：`/home/dsh/.local/conf.ini`，版本包中自带信创服务器的配置文件：`/home/dsh/.local/conf/xinchuang.ini`

!!! warning
    **修改以下配置，对于没有写到的配置和示例中没有写的配置，不要修改和删除!!!**

    **修改以下配置，对于没有写到的配置和示例中没有写的配置，不要修改和删除!!!**

    **修改以下配置，对于没有写到的配置和示例中没有写的配置，不要修改和删除!!!**

    
### 使用信创服务器的配置文件
使用信创的配置文件覆盖 `conf.ini`，**后续修改的配置均在 `conf.ini`中进行**
```shell
    cd ~/.local
    sudo cp xinchuang.ini conf.ini
```

### 修改sys模块
-  `device_id`的值应该和 代理B `default.json`中与代理C对应的 `equip`字段相同，即代理C IP地址 `倒数第2段*1000 + 最后1段`，

    !!!example
        代理C IP地址为10.17.1.195，则device_id = 1*1000 + 1195 = 1195

-  `operator` 表示运营商ID，根据实际情况修改
- `daemon` 修改为1

```ini
# 示例
[sys]
device_id = 1195
operator=3
daemon = 1
```

### 修改kafka模块
- `bootstrap.servers` 配置代理A kafka 的IP地址
- `topic` 固定为 `hw-hit`

```ini
# 示例
[kafka]
bootstrap.servers = 10.100.74.13 
topic = hw-hit
```

### 修改tunnel模块
- 修改 `sync_msg_sport`，适配现场同步报文sport
- 修改 `sync_msg_dport`，适配现场同步报文dport

```ini
# 示例
[tunnel]
sync_msg_sport=50631
sync_msg_dport=59883
tunnel_info_timeout=1800

```

### 修改port模块端口mac地址
- 信创的配置文件中，默认绑定两个面板口进行收包，需要为每个口配置一个mac地址，用于从G设备接收U面报文，该mac地址需要与G上配置的dmac地址相同
- 可以使用 `gen_mac` 工具，根据设备 `uuid` 和 `IP地址` 生成两个端口的mac地址，使用方法：
    ```shell
    # 将gen_mac文件上传到任意x86服务器上
    ./gen_mac -i 10.17.1.195 # -i 后跟设备IP地址
    port0 mac = 00:25:a9:c1:46:16
    port1 mac = 00:25:a9:c1:46:17
    ```


!!!warning
    1. 每个代理C的每个端口，mac地址建议不重复
    2. mac地址不建议配置为组播地址
   

```ini
# 示例
port0_mac=94:65:9c:0c:ca:39

port1_mac=94:65:9c:0c:ca:38

```


!!!note "组播地址简介"
    ![组播mac地址](assets/组播mac地址.png)

!!!example "G配置示例"
    ```ini
    add outgroup 1 10/f/14
    add outgroup 2 10/f/16
    set action 10 loopback hash sd
    set action 12 copyto outgroup 1 hash sd
    set action 13 copyto outgroup 2 hash sd
    set link 1 upstream 10/f/13 downstream 10/f/15
    set link 1 enable
    set interface 10/f/14 mac=94:65:9c:0c:ca:38
    set interface 10/f/16 mac=94:65:9c:0c:ca:39
    add ingroup 1 10/f/13
    add ingroup 2 10/f/15
    
    ```


## 启动并检查代理C
执行以下命令启动并检查代理C状态，预期状态为active (running)，首次启动较长。
```shell
sudo systemctl start ntv
sudo systemctl status ntv 
```

# 安装代理B

## 上传并解压软件包

- 上传软件包到 `/home/dsh/proxy_b/verison`目录下

- 解压软件包

    ```shell
    cd /home/dsh/proxy_b/version
    tar -xzvf -C ~/proxy_b
    ```

## 开启systemctl功能

```shell
sudo cp /home/dsh/proxy_b/multi_process_fz/fz-agent.service /usr/lib/systemd/system
sudo systemctl enable fz-agent
```

## 修改代理B配置文件-default.json

### 配置equip_prefix、equip、equip_type

这三个参数用于配置G或者代理C

- `equip_prefix`： 指G或代理C IP地址的前两段
- `equip` ：指G或代理C IP地址后面两位经过计算的结果，计算方法：倒数第2位*1000 + 最后1位
- `equip_type`： 设备类型，支持的类型：G、C，分别表示G设备和代理C

!!! example

    假设1台G和1台代理C， IP地址分别为 `10.17.1.93` 和 `10.17.1.195`，则三个参数分别为
    
    - "equip_prefix":"10.17,10.17",
    - "equip" : "1093,1195",
    - "equip_type":"G,C",

### 配置kafka地址

- `bootstrap` ：表示代理A的kafka IP地址和端口号，一般端口号为`9092`

### 配置老化时间和轮询时间

- `aging_time`： 表示代理B给G下发的`ip+appid`规则的老化时间，参照一期，设置为`180s`
- `flush_time`：表示代理B从代理A轮询规则的间隔时间，参考一期，设置为`60s`

### 配置全量更新ftp服务器

以下三个参数，用于配置 存放全量更新has文件的ftp服务器地址，ftp服务器理论上，由客户提供

- `ftp_server`：ftp服务器IP地址
- `ftp_user`：ftp用户名
- `ftp_passwd`：ftp密码

### default.json样例

```json

{
  "type":"dsh",
  "equip_prefix":"10.17,10.17",
  "equip" : "1093,1195",
  "equip_type":"G,C",
  "bootstrap" : "10.100.74.13:9092",
  "port" : 7778,
  "operator": 3,
  "aging_time":180,
  "flush_time":60,
  "fz_reload": {
    "topic": "hw-whitelist",
    "code": "11",
    "ftp_server": "10.100.74.133",
    "ftp_user": "wj",
    "ftp_passwd": "123",
    "ssh_name": "wenjun",
    "ssh_passwd": "123",
    "reset_seq": 1
  }
}
```

## 启动并检查代理B

执行以下指令启动代理B，并检查代理B状态

```shell
# 启动代理B
sudo systemctl start fz-agent

# 检查代理B状态
[dsh@localhost ~]$ sudo systemctl status fz-agent 
● fz-agent.service - fz agent service
   Loaded: loaded (/usr/lib/systemd/system/fz-agent.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2022-09-06 16:06:27 CST; 13s ago
 Main PID: 8273 (python3)
    Tasks: 23
   CGroup: /system.slice/fz-agent.service
           ├─8273 /usr/bin/python3 -u main.py
           ├─8274 /usr/bin/python3 -u main.py
           ├─8275 /usr/bin/python3 -u main.py
           ├─8276 /usr/bin/python3 -u main.py
           ├─8277 /usr/bin/python3 -u main.py
           └─8278 /usr/bin/python3 -u main.py

Sep 06 16:06:27 localhost.localdomain systemd[1]: Started fz agent service.
Sep 06 16:06:28 localhost.localdomain python3[8273]: Start recv pkt
Sep 06 16:06:28 localhost.localdomain python3[8273]: new shelve db 1195
Sep 06 16:06:28 localhost.localdomain python3[8273]: new shelve db 1093

```

# 升级代理C
- 将新的 `rpm`包放到 `/home/dsh/proxy_c/version`目录下
- 停止代理C
    ```shell
    sudo systemctl stop ntv
    
    ```
- 安装新的`rpm`包
    ```shell
    cd ~/proxy_c/version
    sudo rpm -Uvh new_proxy_c.rpm --nodeps --prefix=/home/dsh/.local
    
    ```
- 重启代理C并检查代理C状态
    ```shell
    sudo systemctl restart ntv
    sudo systemctl status ntv 
    
    ```
- 检查代理C版本
    ```shell
    [dsh@localhost bin]$ ntv_cli
    CLI> show version 
    CMUD-CM 1.0.3 (dsh2-fz-6c87f08 build 0 2022-09-09)
    CLI> quit
    [dsh@localhost bin]$ 
    ```

# 升级代理B
- 将新的软件包放到 `/home/dsh/proxy_b/version`目录下，校验MD5
- 停止代理B
    ```shell
    sudo systemctl stop fz-agent
    
    ```
- 判断规则文件是否有更新，如果没有更新，则跳过本步骤，如果有更新，则执行以下指令，删除旧的规则文件
    ```shell
    sudo rm -rf /home/dsh/proxy_b/multi_process_fz/rule/*
    ```
- 解压软件包
    ```shell
    cd /home/dsh/proxy_b/version
    tar -xzvf new_proxy_b.tar.gz --exclude=multi_process_fz/default.json  -C /home/dsh/proxy_b
    ```
- 检查代理B版本号
    ```shell
    cd /home/dsh/proxy_b/multi_process_fz
    sh version.sh version
    
    ```
- 重启代理B并检查代理B状态
    ```shell
    sudo systemctl restart fz-agent
    sudo systemctl status fz-agent
    ```
