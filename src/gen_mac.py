#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
Author       : zouxuan
Date         : 2022-09-13 15:37:53
LastEditTime : 2022-09-13 16:28:20
LastEditors  : zouxuan
Description  :
FilePath     : gen_mac.py
'''

import uuid
import argparse
import socket
import struct
import re


def ip2long(ip):  # ip地址转长整型
    packedIP = socket.inet_aton(ip)
    return struct.unpack("!L", packedIP)[0]


def long2ip(long):  # 长整型转IP地址
    ip = socket.inet_ntoa(struct.pack('!L', long))
    # print('{} 对应的IP地址为: {}'.format(long, ip))
    return ip


# https://ouilookup.com/search/embedway
embedway_oui = '00:25:a9'


def main(args):
    cur_uuid = int(hex(uuid.getnode()).split('0x')[-1][6:], 16)
    long_ip = ip2long(args.ip)
    mac_port0 = hex(cur_uuid + long_ip).split('0x')[-1][-6:]
    mac_port0 = ':'.join(re.findall(r'\w{2}', mac_port0))
    mac_port0 = embedway_oui + ':' + mac_port0
    mac_port1 = hex(cur_uuid + long_ip + 1).split('0x')[-1][-6:]
    mac_port1 = ':'.join(re.findall(r'\w{2}', mac_port1))
    mac_port1 = embedway_oui + ':' + mac_port1
    print(f'port0 mac = {mac_port0}')
    print(f'port1 mac = {mac_port1}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='''代理C mac地址生成工具，生成规则：
1. mac地址前3个字节为恒为oui
2. port0 mac地址后三个字节为uuid后三个字节+ip地址的结果，取后三个字节，port1在port0的基础上加1''', epilog='''
注意：
生成的mac地址建议做好记录，连接到同一个交换机的多个代理C之间，不能有重复的mac地址，在生成后，查询下mac地址是否已经使用过''', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-i', dest='ip', required=True, help='当前设备IPv4地址, 如10.17.1.195')
    main(parser.parse_args())
